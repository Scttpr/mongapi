import axios, { AxiosInstance } from "axios";
import { QueryBuilder } from "./QueryBuilder";

export class Connector {
  protected readonly http: AxiosInstance = axios;

  constructor(private readonly host: string) {}

  protected getBaseUrl(target: string): string {
    return `${this.host}/${target}`;
  }

  public newEntity(entityName: string): QueryBuilder {
    return new QueryBuilder(this.http, this.getBaseUrl(entityName));
  }
}
