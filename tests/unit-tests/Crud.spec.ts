import { expect } from 'chai';

import { ComposedAbstractQueryBuilder } from "../../src/abstracts/AbstractQueryBuilder";
import { DELETE, GET, PATCH, POST, PUT } from "../../src/abstracts/Crud";
import { DUMMY_BODY, DUMMY_ID } from "../tools/constants";

describe('Testing QueryCrudMixin', () => {
  class DummyClass extends ComposedAbstractQueryBuilder {
      [x: string]: any;
  }

  describe('Class and properties', () => {
    it('should have a all crud methods', () => {
      const entity = new DummyClass();
      const methodsNames = ['get', 'list', 'create', 'update', 'replace', 'remove'];

      methodsNames.forEach((methodName) => {
        expect(entity).to.respondTo(methodName);
      });
    });
  });

  describe('get method', () => {
    it('should set configBaseUrl passing given argument as an argument', () => {
      const entity = new DummyClass();
      entity.get(DUMMY_ID);
      const [, id] = entity.getConfigBaseUrl().split('/');
      expect(id).to.equal(DUMMY_ID);
    });

    it('should set config.method to GET', () => {
      const entity = new DummyClass();
      entity.get(DUMMY_ID);
      expect(entity)
        .to.have.nested.property('config.method')
        .that.equal(GET);
    });

    it('should return itself', () => {
      const entity = new DummyClass();
      expect(entity.get(DUMMY_ID)).to.be.an.instanceOf(DummyClass);
    });
  });

  describe('list method', () => {
    it('should set config.method to GET', () => {
      const entity = new DummyClass();
      entity.list();
      expect(entity)
        .to.have.nested.property('config.method')
        .that.equal(GET);
    });

    it('should return itself', () => {
      const entity = new DummyClass();
      expect(entity.list()).to.be.an.instanceOf(DummyClass);
    });
  });

  describe('create method', () => {
    it('should set config.method to POST', () => {
      const entity = new DummyClass();
      entity.create(DUMMY_BODY);
      expect(entity)
        .to.have.nested.property('config.method')
        .that.equal(POST);
    });

    it('should only accept object as an argument', () => {
      const entity = new DummyClass();
      const returnMethod = (arg?: unknown) => () => entity.create(arg);

      expect(returnMethod()).to.throw();
      expect(returnMethod('dummy_string')).to.throw();
      expect(returnMethod(123)).to.throw();
      expect(returnMethod(['dummy_value', 54565])).to.throw();
      expect(returnMethod(() => {
        // Execute some dummy code
      })).to.throw();
      expect(returnMethod(null)).to.throw();
      expect(returnMethod(Symbol())).to.throw();
    });

    it('should set config.data with given argument', () => {
      const entity = new DummyClass();
      entity.create(DUMMY_BODY);
      expect(entity)
        .to.have.nested.property('config.data')
        .that.has.all.keys(Object.keys(DUMMY_BODY));
    });

    it('should return itself', () => {
      const entity = new DummyClass();
      expect(entity.create(DUMMY_BODY)).to.be.an.instanceOf(DummyClass);
    });
  });

  describe('update method', () => {
    it('should set configBaseUrl passing given argument as an argument', () => {
      const entity = new DummyClass();
      entity.update(DUMMY_ID, DUMMY_BODY);
      const [, id] = entity.getConfigBaseUrl().split('/');
      expect(id).to.equal(DUMMY_ID);
    });

    it('should only accept object for body', () => {
      const entity = new DummyClass();
      const returnMethod = (id: string, body?: unknown) => () => entity.update(id, body);

      expect(returnMethod(DUMMY_ID)).to.throw();
      expect(returnMethod(DUMMY_ID, 'dummy_string')).to.throw();
      expect(returnMethod(DUMMY_ID, 123)).to.throw();
      expect(returnMethod(DUMMY_ID, ['dummy_value', 54565])).to.throw();
      expect(returnMethod(DUMMY_ID, () => {
        // Execute some dummy code
      })).to.throw();
      expect(returnMethod(DUMMY_ID, null)).to.throw();
      expect(returnMethod(DUMMY_ID, Symbol())).to.throw();
    });

    it('should set config.data with given argument', () => {
      const entity = new DummyClass();
      entity.update(DUMMY_ID, DUMMY_BODY);
      expect(entity)
        .to.have.nested.property('config.data')
        .that.has.all.keys(Object.keys(DUMMY_BODY));
    });

    it('should set config.method to PATCH', () => {
      const entity = new DummyClass();
      entity.update(DUMMY_ID, DUMMY_BODY);
      expect(entity)
        .to.have.nested.property('config.method')
        .that.equal(PATCH);
    });

    it('should return itself', () => {
      const entity = new DummyClass();
      expect(entity.update(DUMMY_ID, DUMMY_BODY)).to.be.an.instanceOf(DummyClass);
    });
  });

  describe('replace method', () => {
    it('should set configBaseUrl passing given argument as an argument', () => {
      const entity = new DummyClass();
      entity.replace(DUMMY_ID, DUMMY_BODY);
      const [, id] = entity.getConfigBaseUrl().split('/');
      expect(id).to.equal(DUMMY_ID);
    });

    it('should only accept object for body', () => {
      const entity = new DummyClass();
      const returnMethod = (id: string, body?: unknown) => () => entity.replace(id, body);

      expect(returnMethod(DUMMY_ID)).to.throw();
      expect(returnMethod(DUMMY_ID, 'dummy_string')).to.throw();
      expect(returnMethod(DUMMY_ID, 123)).to.throw();
      expect(returnMethod(DUMMY_ID, ['dummy_value', 54565])).to.throw();
      expect(returnMethod(DUMMY_ID, () => {
        // Execute some dummy code
      })).to.throw();
      expect(returnMethod(DUMMY_ID, null)).to.throw();
      expect(returnMethod(DUMMY_ID, Symbol())).to.throw();
    });

    it('should set config.data with given argument', () => {
      const entity = new DummyClass();
      entity.update(DUMMY_ID, DUMMY_BODY);
      expect(entity)
        .to.have.nested.property('config.data')
        .that.has.all.keys(Object.keys(DUMMY_BODY));
    });

    it('should set config.method to PUT', () => {
      const entity = new DummyClass();
      entity.replace(DUMMY_ID, DUMMY_BODY);
      expect(entity)
        .to.have.nested.property('config.method')
        .that.equal(PUT);
    });

    it('should return itself', () => {
      const entity = new DummyClass();
      expect(entity.replace(DUMMY_ID, DUMMY_BODY)).to.be.an.instanceOf(DummyClass);
    });
  });

  describe('remove method', () => {
    it('should set configBaseUrl passing given argument as an argument', () => {
      const entity = new DummyClass();
      entity.remove(DUMMY_ID);
      const [, id] = entity.getConfigBaseUrl().split('/');
      expect(id).to.equal(DUMMY_ID);
    });

    it('should set config.method to DELETE', () => {
      const entity = new DummyClass();
      entity.remove(DUMMY_ID);
      expect(entity)
        .to.have.nested.property('config.method')
        .that.equal(DELETE);
    });
    it('should return itself', () => {
      const entity = new DummyClass();
      expect(entity.remove()).to.be.an.instanceOf(DummyClass);
    });
  });
});
