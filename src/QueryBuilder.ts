import { ComposedAbstractQueryBuilder } from "./abstracts/AbstractQueryBuilder";
import { AxiosInstance, AxiosResponse } from "axios";

export class QueryBuilder extends ComposedAbstractQueryBuilder {
  constructor(private readonly http: AxiosInstance, protected readonly endpoint: string) {
    super();
    this.setConfigBaseUrl();
  }

  public async call(): Promise<AxiosResponse> {
    if (!this.config.method) {
      throw new Error('Method should be provided');
    }
    if (!this.config.baseURL) {
      throw new Error('BaseURL should be provided');
    }
    const res = await this.http(this.config);
    this.config = { baseURL: this.endpoint };
    return res;
  }
}
