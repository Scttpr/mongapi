# Mongapi

![GitHub package.json version](https://img.shields.io/github/package-json/v/Mobility-Recorder/mongapi)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)
[![pipeline status](https://gitlab.com/Scttpr/mongapi/badges/master/pipeline.svg)](https://gitlab.com/Scttpr/mongapi/-/commits/master)
[![coverage report](https://gitlab.com/Scttpr/mongapi/badges/master/coverage.svg)](https://gitlab.com/Scttpr/mongapi/-/commits/master)

Mongapi is a fluent query builder for MongoDB RESTful APIs.
It allows to build complex HTTP requests to query RESTful APIs based on MongoDB :

```js
// mongapi instance allow to set connection through Axios
const mongapi = new Mongapi('api_url');

// Entities are actual query builder instances pointing towards the given resource
const User = mongapi.newEntity('users');

// Some examples, no define orders, request is fired on call
const allUsers = await User.list().call();
const fooUsers = await User.list().setFilter('name', 'foo').call();
const twentyYearOldUsers = await User.setFilter('age', 20).list().call();
```

## Quick documentation
