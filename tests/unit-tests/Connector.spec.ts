import { expect } from 'chai';

import { Connector } from "../../src/Connector";
import { QueryBuilder } from "../../src/QueryBuilder";
import { DUMMY_ENTITY_NAME, DUMMY_HOST, DUMMY_TARGET } from "../tools/constants";

describe('Testing Connector class', () => {
  class DummyConnector extends Connector {
    public getBaseUrl(target: string): string {
      return super.getBaseUrl(target);
    }
  }

  describe('Class and properties', () => {
    it('should take a string parameter and return an instance of itself', () => {
      expect(new Connector(DUMMY_HOST)).to.be.an.instanceOf(Connector);
    });

    it('should have http as a property', () => {
      expect(new Connector(DUMMY_HOST))
        .to.have.property('http');
    });

    it('should have host as a property', () => {
      expect(new Connector(DUMMY_HOST))
        .to.have.property('host')
        .that.is.a('string');
    });

    it('should have getBaseUrl method', () => {
      expect(new Connector(DUMMY_HOST)).to.have.property('getBaseUrl');
    });

    it('should have newEntity method', () => {
      expect(new Connector(DUMMY_HOST)).to.have.property('newEntity');
    });
  });

  describe('getBaseUrl method', () => {
    it('should return a path composed by host and target argument', () => {
      const connector = new DummyConnector(DUMMY_HOST);
      const res = connector.getBaseUrl(DUMMY_TARGET);
      const [host, target] = res.split('/');
      expect(host).to.equal(DUMMY_HOST);
      expect(target).to.equal(DUMMY_TARGET);
    });
  });

  describe('newEntity method', () => {
    it('should return a QueryBuilder with argument as endpoint', () => {
      const connector = new DummyConnector(DUMMY_HOST);
      const entity = connector.newEntity(DUMMY_ENTITY_NAME);
      expect(entity).to.be.an.instanceOf(QueryBuilder);
      expect(entity.getConfigBaseUrl()).to.equal(connector.getBaseUrl(DUMMY_ENTITY_NAME));
    });
  });
});
