import { expect } from 'chai';
import axios, { AxiosRequestConfig } from "axios";

import { QueryBuilder } from "../../src/QueryBuilder";
import { ComposedAbstractQueryBuilder } from "../../src/abstracts/AbstractQueryBuilder";
import { DUMMY_ENDPOINT } from '../tools/constants';

describe('Testing QueryBuilder class', () => {
  describe('Class and properties', () => {
    it('should take an axios instance and a string to return an instance of itself', () => {
      expect(new QueryBuilder(axios, DUMMY_ENDPOINT)).to.be.an.instanceOf(QueryBuilder);
    });

    it('should extend ComposedAbstractQueryBuilder class', () => {
      expect(new QueryBuilder(axios, DUMMY_ENDPOINT))
        .to.be.an.instanceOf(ComposedAbstractQueryBuilder);
    });

    it('should set config base URL on instanciation', () => {
      expect(new QueryBuilder(axios, DUMMY_ENDPOINT))
        .to.have.nested.property('config.baseURL')
        .that.is.equal(DUMMY_ENDPOINT);
    });

    it('should have a call method', () => {
      expect(new QueryBuilder(axios, DUMMY_ENDPOINT)).to.respondTo('call');
    });
  });

  describe('call method', () => {
    it('should return a promise', () => {
      const query = new QueryBuilder(axios, DUMMY_ENDPOINT);
      // @todo handle type issue to remove ts-ignore
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      expect(query.get().call()).to.be.a('promise');
    });

    it('should reset config object excepting baseURL', async () => {
      const query = new QueryBuilder(axios, DUMMY_ENDPOINT);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await query.get().call();

      expect(query)
        .to.have.property('config')
        .that.satisfy((config: AxiosRequestConfig) => {
          return typeof config.baseURL === 'string' && Object.keys(config).length === 1;
      });
    });

    it('should check if HTTP method is set', async () => {
      const query = new QueryBuilder(axios, DUMMY_ENDPOINT);
      let error = null;
      try {
        await query.call();
      } catch (e) {
        error = e;
      }
      expect(error).to.be.an.instanceOf(Error);
      expect(error.message).to.equal('Method should be provided');
    });

    it('should check if base URL is set', async () => {
      class DummyQueryBuilder extends QueryBuilder {
        public resetConfigURL() {
          this.config.baseURL = undefined;
        }
      }
      const query = new DummyQueryBuilder(axios, DUMMY_ENDPOINT);
      query.resetConfigURL();


      let error = null;
      try {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        await query.list().call();
      } catch (e) {
        error = e;
      }
      expect(error).to.be.an.instanceOf(Error);
      expect(error.message).to.equal('BaseURL should be provided');
    });
  });
});
