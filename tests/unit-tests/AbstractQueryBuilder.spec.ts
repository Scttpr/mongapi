import { expect } from 'chai';

import { AbstractQueryBuilder, ComposedAbstractQueryBuilder } from "../../src/abstracts/AbstractQueryBuilder";
import { DUMMY_STRING } from '../tools/constants';

describe('Testing AbstractQueryBuilder', () => {
  describe('Class and properties', () => {
    class DummyChild extends AbstractQueryBuilder {}

    it('should have config property', () => {
      expect(new DummyChild())
        .to.have.property('config')
        .that.is.an('object');
    });

    it('should have endpoint property', () => {
      expect(new DummyChild())
        .to.have.property('endpoint')
        .that.equal('');
    });

    it('should have setConfigBaseUrl method', () => {
      expect(new DummyChild()).to.respondTo('setConfigBaseUrl');
    });
  });

  describe('setConfigBaseUrl method', () => {
    class DummyChild extends AbstractQueryBuilder {
      public setConfigBaseUrl(toAppend?: string) {
        super.setConfigBaseUrl(toAppend);
      }
    }

    it('should set config.baseURL with endpoint value if no arg is provided', () => {
      const entity = new DummyChild();
      entity.setConfigBaseUrl();
      expect(entity.getConfigBaseUrl()).to.equal('');
    });

    it('should append arg to endpoint suffixed with a /', () => {
      const entity = new DummyChild();
      entity.setConfigBaseUrl(DUMMY_STRING);
      expect(entity.getConfigBaseUrl()).to.equal(`/${DUMMY_STRING}`);
    });
  });
});

describe('Testing ComposedAbstractQueryBuilder', () => {
  class DummyChild extends ComposedAbstractQueryBuilder {}

  it('should extend QueryFiltersMixin, QueryCrudMixin', () => {
    const entity = new DummyChild();
    expect(entity).to.respondTo('list');
    expect(entity).to.respondTo('setFilter');
  });

  it('should extend AbstractQueryBuilder', () => {
    const entity = new DummyChild();
    expect(entity).to.respondTo('setConfigBaseUrl');
  });
});
