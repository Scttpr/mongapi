import { expect } from 'chai';

import {
  AbstractFilterBuilder,
  Flags,
  MongoFilter,
  OR
} from "../../src/abstracts/AbstractFilterBuilder";
import { DUMMY_KEY, DUMMY_VALUE, OTHER_DUMMY_KEY } from "../tools/constants";

describe('Testing ComparisonQueryOperators', () => {
  describe('Class and properties', () => {
    class DummyClass extends AbstractFilterBuilder {}

    it('should have ref property', () => {
      expect(new DummyClass())
        .to.have.property('ref')
        .that.is.a('string');
    });

    it('should have key property', () => {
      expect(new DummyClass())
        .to.have.property('key')
        .that.is.a('string');
    });

    it('should have filter property', () => {
      expect(new DummyClass())
        .to.have.property('filter')
        .that.is.an('object');
    });

    it('should have flags property', () => {
      expect(new DummyClass())
        .to.have.property('flags')
        .that.is.an('object');
    });

    it('should have is getter', () => {
      const filter = new DummyClass();
      filter.where(DUMMY_KEY);

      expect(filter)
        .to.have.property('is')
        .that.is.an.instanceOf(DummyClass);
    });

    it('should have and getter', () => {
      expect(new DummyClass())
        .to.have.property('and')
        .that.is.an.instanceOf(DummyClass);
    });

    it('should have or getter', () => {
      expect(new DummyClass())
        .to.have.property('or')
        .that.is.an.instanceOf(DummyClass);
    });

    it('should have not getter', () => {
      expect(new DummyClass())
        .to.have.property('not')
        .that.is.an.instanceOf(DummyClass);
    });

    it('should have where method', () => {
      expect(new DummyClass()).to.respondTo('where');
    });

    it('should have flag method', () => {
      expect(new DummyClass()).to.respondTo('flag');
    });

    it('should have getFlags method', () => {
      expect(new DummyClass()).to.respondTo('getFlags');
    });

    it('should have isFlag method', () => {
      expect(new DummyClass()).to.respondTo('isFlag');
    });

    it('should have resetFlags method', () => {
      expect(new DummyClass()).to.respondTo('resetFlags');
    });
  });

  class OtherDummyClass extends AbstractFilterBuilder {
    public filter: MongoFilter = {};

    public flag(key: string, value: boolean): void {
      super.flag(key, value);
    }

    public getFlags(): Flags {
      return super.getFlags();
    }

    public isFlag(key: string): boolean {
      return super.isFlag(key);
    }

    public resetFlags() {
      super.resetFlags();
    }
  }

  describe('flag method', () => {
    it('should save value in key property of the flags property', () => {
      const filter = new OtherDummyClass();
      filter.flag(DUMMY_KEY, true);

      expect(filter)
        .to.have.nested.property(`flags.${DUMMY_KEY}`)
        .that.is.equal(true);
    });
  });

  describe('getFlags method', () => {
    it('should return registered flags', () => {
      const filter = new OtherDummyClass();
      filter.flag(DUMMY_KEY, true);

      expect(filter.getFlags())
        .to.have.property(DUMMY_KEY)
        .that.is.equal(true);
    });
  });

  describe('isFlag method', () => {
    it('should return true if flag is true and false if it false or undefined', () => {
      const filter = new OtherDummyClass();
      filter.flag(DUMMY_KEY, true);

      expect(filter.isFlag(DUMMY_KEY)).to.equal(true);
      expect(filter.isFlag(OTHER_DUMMY_KEY)).to.equal(false);

      filter.flag(DUMMY_KEY, false);

      expect(filter.isFlag(DUMMY_KEY)).to.equal(false);
    });
  });

  describe('resetFlags method', () => {
    it('should reset flags to an empty object', () => {
      const filter = new OtherDummyClass();
      filter.resetFlags();

      expect(filter)
        .to.have.property('filter')
        .that.satisfies((prop: Record<string, unknown>) => {
        return Object.keys(prop).length === 0;
      });

      filter.flag(DUMMY_KEY, true);
      filter.resetFlags();

      expect(filter)
        .to.have.property('filter')
        .that.satisfies((prop: Record<string, unknown>) => {
        return Object.keys(prop).length === 0;
      });
    });
  });

  describe('where method', () => {
    it('should return itself', () => {
      const filter = new OtherDummyClass();
      expect(filter.where(DUMMY_KEY)).to.be.equal(filter);
    });

    it('should set key', () => {
      const filter = new OtherDummyClass();
      filter.where(DUMMY_KEY);

      expect(filter)
        .to.have.property('key')
        .that.is.equal(DUMMY_KEY);
    });

    it('should reset flags', () => {
      const filter = new OtherDummyClass();
      filter.flag(DUMMY_KEY, true);
      filter.where(DUMMY_KEY);

      expect(filter)
        .to.have.property('flags')
        .that.satisfies((prop: Record<string, unknown>) => {
        return Object.keys(prop).length === 0;
      });
    });

    it('should throw an error if key is defined in instance but not in instance filter', () => {
      const filter = new OtherDummyClass();
      filter.where(DUMMY_KEY);
      filter.filter.key = '';
      const returnMethod = () => filter.where(OTHER_DUMMY_KEY);
      expect(returnMethod).to.throw('Please provide a value to previous assertion');
    });

    it('should add $or property with existing key and a null value on given key property if or flag is set', () => {
      const filter = new OtherDummyClass();
      filter.where(DUMMY_KEY);
      filter.filter[DUMMY_KEY] = DUMMY_VALUE;
      filter.flag(OR, true);
      filter.where(OTHER_DUMMY_KEY);

      expect(filter)
        .to.have.property('filter')
        .that.has.property('$or')
        .that.eql([
          { [DUMMY_KEY]: DUMMY_VALUE },
          { [OTHER_DUMMY_KEY]: null }
        ]);

      expect(filter).to.not.have.property(DUMMY_KEY);
    });
  });

  describe('is getter', () => {
    it('should throw an error if key is not defined', () => {
      const filter = new OtherDummyClass();
      const returnGetter = () => filter.is;
      expect(returnGetter).to.throw('Key must be defined');
    });
  });
});
