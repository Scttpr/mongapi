import { AbstractFilterBuilder, NEGATE } from "./AbstractFilterBuilder";

type ComparisonValue = string | number;

interface ComparisonOperators {
  equal: (value: ComparisonValue) => this;
  greater: (value: ComparisonValue) => this;
  lesser: (value: ComparisonValue) => this;
  in: (values: Array<ComparisonValue>) => this;
}

export const ComparisonQueryOperatorsMixin = (base: typeof AbstractFilterBuilder): typeof AbstractFilterBuilder => class ComparisonQueryOperators extends base implements ComparisonOperators {
  public equal(value: ComparisonValue): this {
    if (this.isFlag(NEGATE)) {
      this.filter[this.key] = { $ne: value };
    } else {
      this.filter[this.key] = value;
    }
    return this;
  }

  public greater(value: ComparisonValue): this {
    if (this.isFlag(NEGATE)) {
      this.filter[this.key] = { $lte: value };
    } else {
      this.filter[this.key] = { $gt: value };
    }
    return this;
  }

  public lesser(value: ComparisonValue): this {
    if (this.isFlag(NEGATE)) {
      this.filter[this.key] = { $gte: value };
    } else {
      this.filter[this.key] = { $lt: value };
    }
    return this;
  }

  public in(values: Array<ComparisonValue>): this {
    if (this.isFlag(NEGATE)) {
      this.filter[this.key] = { $nin: values };
    } else {
      this.filter[this.key] = { $in: values };
    }
    return this;
  }
};
