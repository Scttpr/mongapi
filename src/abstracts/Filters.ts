import { AbstractQueryBuilder } from "./AbstractQueryBuilder";

export const QueryFiltersMixin = (base: typeof AbstractQueryBuilder): typeof AbstractQueryBuilder => class QueryFilters extends base {
  setFilter(key: string, condition: string): this {
    if (typeof this.config.params === "undefined") {
      this.config.params = { filters: {} };
    }
    this.config.params.filters[key] = condition;

    return this;
  }
};
