import { compose } from 'lodash/fp';
import { QueryFiltersMixin } from "./Filters";
import { QueryCrudMixin } from "./Crud";
import { AxiosRequestConfig } from "axios";

export abstract class AbstractQueryBuilder {
  protected config: AxiosRequestConfig = {};
  protected endpoint = '';

  protected setConfigBaseUrl(toAppend?: string): void {
    if (typeof toAppend === 'string') {
      this.config.baseURL = `${this.endpoint}/${toAppend}`;
    } else {
      this.config.baseURL = this.endpoint;
    }
  }

  // @todo check if needed outside of tests
  public getConfigBaseUrl(): string {
    return this.config.baseURL || '';
  }
}

export const ComposedAbstractQueryBuilder = compose(QueryFiltersMixin, QueryCrudMixin)(AbstractQueryBuilder);
