import { expect } from 'chai';

import { ComposedAbstractQueryBuilder } from "../../src/abstracts/AbstractQueryBuilder";
import { DUMMY_CONDITION, DUMMY_KEY } from "../tools/constants";

describe('Testing QueryFilterMixin', () => {
  class DummyClass extends ComposedAbstractQueryBuilder {
      [x: string]: any;
  }


  describe('Class and properties', () => {
    it('should have setFilter method', () => {
      expect(new DummyClass()).to.respondTo('setFilter');
    });
  });

  describe('setFilter method', () => {
    it('should set a filter if no filters are already defined', () => {
      const entity = new DummyClass();
      entity.setFilter(DUMMY_KEY, DUMMY_CONDITION);
      expect(entity)
        .to.have.nested.property('config.params.filters')
        .that.has.property(DUMMY_KEY)
        .that.equal(DUMMY_CONDITION);
    });

    it('should set a filter and keep the existing one', () => {
      const entity = new DummyClass();
      const OTHER_DUMMY_KEY = 'other_dummy_key';
      const OTHER_DUMMY_CONDITION = 'other_dummy_condition';
      entity.setFilter(DUMMY_KEY, DUMMY_CONDITION);
      entity.setFilter(OTHER_DUMMY_KEY, OTHER_DUMMY_CONDITION);

      expect(entity)
        .to.have.nested.property('config.params.filters')
        .that.has.all.keys(DUMMY_KEY, OTHER_DUMMY_KEY);
    });

    it('should return itself', () => {
      const entity = new DummyClass();
      expect(entity.setFilter(DUMMY_KEY, DUMMY_CONDITION)).to.be.an.instanceOf(DummyClass);
    });
  });
});
