// @todo mock server call here
export const DUMMY_ENDPOINT = 'https://jsonplaceholder.typicode.com/todos';
export const DUMMY_KEY = 'dummy_key';
export const OTHER_DUMMY_KEY = 'other_dummy_key';
export const DUMMY_VALUE = 'dummy_value';
export const OTHER_DUMMY_VALUE = 'other_dummy_value';
export const DUMMY_VALUES = [DUMMY_VALUE, OTHER_DUMMY_VALUE];
export const DUMMY_HOST = 'some_dummy_host';
export const DUMMY_TARGET = 'some_dummy_target';
export const DUMMY_ENTITY_NAME = 'some_dummy_name';
export const DUMMY_STRING = 'some_dummy_text_to_append';
export const DUMMY_ID = 'dummy_id';
export const DUMMY_BODY = {
  dummyKey: 'dummyData',
  otherDummyKey: 'otherDummyData'
};
export const DUMMY_CONDITION = 'dummy-condition';

