export const isArray = (value?: unknown): boolean => typeof value === 'object' && value !== null && value.constructor === Array;
export const isObjectLiteral = (value?: unknown): boolean => typeof value === 'object' && value !== null && value.constructor === Object;
