import { expect } from 'chai';

import { isArray, isObjectLiteral } from "../../src/tools/typeValidation";

const allOtherTypes = [
  undefined,
  'string',
  12345,
  true,
  Symbol(),
  null,
  () => {
    // some code to process,
  },
];

const dummyArray = [1, 2, false];
const dummyObject = { key: 'value' };

describe('Testing isArray utility function', () => {
  it('should return true only if the value is an array', () => {
    allOtherTypes.forEach((value) => {
      expect(isArray(value)).to.equal(false);
    });

    expect(isArray()).to.equal(false);
    expect(isArray(dummyObject)).to.equal(false);
    expect(isArray(dummyArray)).to.equal(true);
  });
});

describe('Testing isObjectLiteral utility function', () => {
  it('should return true only if the value is an object', () => {
    allOtherTypes.forEach((value) => {
      expect(isObjectLiteral(value)).to.equal(false);
    });

    expect(isObjectLiteral()).to.equal(false);
    expect(isObjectLiteral(dummyArray)).to.equal(false);
    expect(isObjectLiteral(dummyObject)).to.equal(true);
  });
});
