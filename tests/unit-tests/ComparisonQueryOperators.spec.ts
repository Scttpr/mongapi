import { expect } from 'chai';
import { compose } from 'lodash/fp';

import { ComparisonQueryOperatorsMixin } from "../../src/abstracts/ComparisonQueryOperators";
import { AbstractFilterBuilder, Flags } from "../../src/abstracts/AbstractFilterBuilder";
import { DUMMY_KEY, DUMMY_VALUE, DUMMY_VALUES } from "../tools/constants";

describe('Testing ComparisonQueryOperators', () => {
  class DummyClass extends compose(ComparisonQueryOperatorsMixin)(AbstractFilterBuilder) {}

  describe('Class and properties', () => {
    it('should have equal method', () => {
      expect(new DummyClass()).to.respondTo('equal');
    });

    it('should have greater method', () => {
      expect(new DummyClass()).to.respondTo('greater');
    });

    it('should have lesser method', () => {
      expect(new DummyClass()).to.respondTo('lesser');
    });

    it('should have in method', () => {
      expect(new DummyClass()).to.respondTo('in');
    });
  });

  describe('equal method', () => {
    it('should return itself', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      expect(filter.equal(DUMMY_VALUE)).to.be.an.instanceOf(DummyClass);
    });

    it('should assign given argument to current defined key', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.equal(DUMMY_VALUE);
      expect(filter.filter[DUMMY_KEY]).to.equal(DUMMY_VALUE);
    });

    it('should assign given argument to current defined key with a $ne if negate flag is on', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.flags.negate = true;
      filter.equal(DUMMY_VALUE);

      expect(filter.filter[DUMMY_KEY]).to.eql({ $ne: DUMMY_VALUE });
    });
  });

  describe('greater method', () => {
    it('should return itself', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      expect(filter.greater(DUMMY_VALUE)).to.be.an.instanceOf(DummyClass);
    });

    it('should assign given argument to current defined key preprended by $gt', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.greater(DUMMY_VALUE);
      expect(filter.filter[DUMMY_KEY]).to.eql({ $gt: DUMMY_VALUE });
    });

    it('should assign given argument to current defined key preprended by $lte if negate flag is on', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.flags.negate = true;
      filter.greater(DUMMY_VALUE);
      expect(filter.filter[DUMMY_KEY]).to.eql({ $lte: DUMMY_VALUE });
    });
  });

  describe('lesser method', () => {
    it('should return itself', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      expect(filter.lesser(DUMMY_VALUE)).to.be.an.instanceOf(DummyClass);
    });

    it('should assign given argument to current defined key preprended by $lt', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.lesser(DUMMY_VALUE);
      expect(filter.filter[DUMMY_KEY]).to.eql({ $lt: DUMMY_VALUE });
    });

    it('should assign given argument to current defined key preprended by $gte if negate flag is on', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.flags.negate = true;
      filter.lesser(DUMMY_VALUE);
      expect(filter.filter[DUMMY_KEY]).to.eql({ $gte: DUMMY_VALUE });
    });
  });

  describe('in method', () => {
    it('should return itself', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      expect(filter.in(DUMMY_VALUE)).to.be.an.instanceOf(DummyClass);
    });

    it('should assign given argument to current defined key preprended by $in', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.in(DUMMY_VALUES);
      expect(filter.filter[DUMMY_KEY]).to.eql({ $in: DUMMY_VALUES });
    });

    it('should assign given argument to current defined key preprended by $nin if negate flag is on', () => {
      const filter = new DummyClass();
      filter.key = DUMMY_KEY;
      filter.flags.negate = true;
      filter.in(DUMMY_VALUES);
      expect(filter.filter[DUMMY_KEY]).to.eql({ $nin: DUMMY_VALUES });
    });
  });

});
