import { AbstractQueryBuilder } from "./AbstractQueryBuilder";
import { isObjectLiteral } from "../tools/typeValidation";

export const GET = 'GET';
export const POST = 'POST';
export const PUT = 'PUT';
export const PATCH = 'PATCH';
export const DELETE = 'DELETE';

export const QueryCrudMixin = (base: typeof AbstractQueryBuilder): typeof AbstractQueryBuilder => class QueryCrud extends base {

  public get(id: string): this {
    this.setConfigBaseUrl(id);
    this.config.method = GET;

    return this;
  }

  public list(): this {
    this.config.method = GET;

    return this;
  }

  public create(body: Record<string, any>): this {
    if (!isObjectLiteral(body)) {
      throw new Error('Provided body should be an object');
    }

    this.config.data = body;
    this.config.method = POST;

    return this;
  }

  public update(id: string, body: Record<string, any>): this {
    if (!isObjectLiteral(body)) {
      throw new Error('Provided body should be an object');
    }

    this.setConfigBaseUrl(id);
    this.config.data = body;
    this.config.method = PATCH;

    return this;
  }

  public replace(id: string, body: Record<string, any>): this {
    if (!isObjectLiteral(body)) {
      throw new Error('Provided body should be an object');
    }

    this.setConfigBaseUrl(id);
    this.config.data = body;
    this.config.method = PUT;

    return this;
  }

  public remove(id: string): this {
    this.setConfigBaseUrl(id);
    this.config.method = DELETE;

    return this;
  }
};
