import { compose } from "lodash/fp";
import { ComparisonQueryOperatorsMixin } from "./ComparisonQueryOperators";

export interface Flags {
  [key: string]: boolean;
}

export interface MongoFilter {
  [key: string]: null | string | number | MongoFilter | Array<string | number | MongoFilter>
}

interface BaseFilter {
  where: (key: string) => this;
  is: this;
  or: this;
  and: this;
  not: this;
}

export const NEGATE = 'negate';
export const OR = 'or';
export const AND = 'and';

export abstract class AbstractFilterBuilder implements BaseFilter {
  protected filter: MongoFilter = {}
  protected flags: Flags = {}
  protected key = '';
  protected ref = '';

  protected flag(key: string, value: boolean): void {
      this.flags[key] = value;
  }

  protected getFlags(): Flags {
    return this.flags;
  }

  protected isFlag(key: string): boolean {
    return this.flags[key] || false;
  }

  protected resetFlags(): void {
    this.flags = {};
  }

  where(key: string): this {
    if (this.key && !this.filter[this.key]) {
      throw new Error('Please provide a value to previous assertion');
    }

    // @todo implement AND condition
    if (this.isFlag(OR)) {
      this.filter.$or = [
        { [this.key]: this.filter[this.key] },
        { [key]: null },
      ];
      delete this.filter[this.key];
    } else if (this.isFlag(AND)) {
      // @todo implement cases $and is required (multiple $or)
    }
    this.resetFlags();
    this.key = key;
    return this;
  }

  get is(): this {
    if (!this.key) {
      throw new Error('Key must be defined');
    }
    return this;
  }

  get and(): this {
    this.flag(AND, true);
    return this;
  }

  get or(): this {
    this.flag(OR, true);
    return this;
  }

  get not(): this {
    this.flag(NEGATE, true);
    return this;
  }
}

export const ComposedAbstractFilterBuilder = compose(ComparisonQueryOperatorsMixin)(AbstractFilterBuilder);
